import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Biodata Diri',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const Biodata(),
    );
  }
}

class Biodata extends StatelessWidget {
  const Biodata({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.blue[800],
        title: Text('My Biodata | ITB Palcomtech'),
      ),
      body: SafeArea(
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.all(10),
              height: 250,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                image: DecorationImage(
                  image: AssetImage('images/profile.png'),
                  fit: BoxFit.contain
                ),
              ),
            ),
            Container(
              alignment: Alignment.centerLeft,
              margin: EdgeInsets.only(left: 10),
              child: Text('Muhammad Julian Saputra',
                style: TextStyle(
                  color: Colors.blue[800],
                  fontSize: 35
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(10),
              child: Container(
                height: 2,
                color: Colors.blue[800],
              ),
            ),
            Container(
              alignment: Alignment.centerLeft,
              margin: EdgeInsets.only(left: 10),
              child: Text('031200012',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 20,
                ),
              ),
            ),
            Container(
              alignment: Alignment.centerLeft,
              margin: EdgeInsets.only(left: 10),
              child: Text('D3 Sistem Informasi',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 20
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(10),
              child: Container(
                height: 2,
                color: Colors.blue[800],
              ),
            ),
            Container(
              alignment: Alignment.centerLeft,
              margin: EdgeInsets.only(left: 10),
              child: Text('081933511948',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 20
                ),
              ),
            ),
            Container(
              alignment: Alignment.centerLeft,
              margin: EdgeInsets.only(left: 10),
              child: Text('Palembang, 02 Juli 2002',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 20
                ),
              ),
            ),
            Container(
              alignment: Alignment.centerLeft,
              margin: EdgeInsets.only(left: 10),
              child: Text('Laki-laki',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 20
                ),
              ),
            ),
            Container(
              alignment: Alignment.centerLeft,
              margin: EdgeInsets.only(left: 10),
              child: Text('Islam',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 20
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}